package com.example.deeplink_first_app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.deeplink_first_app.ui.theme.DeeplinkfirstappTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DeeplinkfirstappTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    TestLayout()
                }
            }
        }
    }
}

@Composable
fun TestButton(name: String, viewModel: MainViewModel) {
    val handler = LocalUriHandler.current
    Button(onClick = {
        viewModel.onSubmit(handler)
    }) {
        Text(text = name)
    }
}

@Composable
fun Form(viewModel: MainViewModel) {

    val text: String by viewModel.text.observeAsState(initial = "")
    val number: String by viewModel.number.observeAsState(initial = "0")

    TextField(
        value = text,
        label = { Text("Text") },
        onValueChange = {
            viewModel.onChange(it, number)
        },
        maxLines = 1
    )
    Spacer(modifier = Modifier.padding(10.dp))
    TextField(
        value = number,
        label = { Text("Number") },
        onValueChange = {
            viewModel.onChange(text, it)
        },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        maxLines = 1,
    )
}

@Composable
fun TestLayout(viewModel: MainViewModel = MainViewModel()) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Form(viewModel)
        TestButton("Go to second app", viewModel)
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DeeplinkfirstappTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colors.background
        ) {
            TestLayout()
        }
    }
}