package com.example.deeplink_first_app

import androidx.compose.ui.platform.UriHandler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class MainViewModel {

    private val _text: MutableLiveData<String> = MutableLiveData<String>()
    val text: LiveData<String> = _text;

    private val _number: MutableLiveData<String> = MutableLiveData<String>()
    val number: LiveData<String> = _number;

    fun onChange(text: String, number: String) {
        _text.value = text;
        _number.value = number;

    }

    fun onSubmit(handler: UriHandler){
        val url: String = "https://www." + "felipevogt.com/secondApp?text=${_text.value}&number=${_number.value}";
        println("${url}")
        handler.openUri(url);
    }
}